package org.goejl.core.websocket;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// Inspiration :: http://saurabh29july.blog/understanding-websockets-using-java-embedded-jetty-server-a-simple-html-javascript-client/
public class TheWebSocketServerImpl {
  private static final Logger logger = LoggerFactory.getLogger(TheWebSocketServerImpl.class);
  public static void main(String[] args) throws Exception {
    Server server = new Server(8099);
    WebSocketHandler wsHandler = new WebSocketHandler() {
      @Override
      public void configure(WebSocketServletFactory factory) {
        factory.register(TheWebSocketHandler.class);
      }
    };
    server.setHandler(wsHandler);
    server.start();
    logger.info("Server Started !!");
    server.join();
  }
}