package org.goejl.core.websocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebSocket
public class TheWebSocketHandler {
  private static final Logger logger = LoggerFactory.getLogger(TheWebSocketHandler.class);
  @OnWebSocketClose
  public void onClose(int statusCode, String reason) {
    logger.info("Close: statusCode={}, reason={}", statusCode, reason);
  }

  @OnWebSocketError
  public void onError(Throwable t) {
    logger.error("Error: {}", t.getMessage());
  }

  @OnWebSocketConnect
  public void onConnect(Session session) {
    logger.info("Connect: {}", session.getRemoteAddress().getAddress());
    try {
      session.getRemote().sendString("Pong from WS Server!");
      ReceiverThread receiverThread = new ReceiverThread();
      receiverThread.setSession(session);
      receiverThread.start();
    } catch (IOException e) {
      logger.error("Exception: ", e);
    }
  }

  @OnWebSocketMessage
  public void onMessage(Session session, InputStream in) {
    logger.info("Received a new call InputStream!");
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    try{
      while(reader.ready()){
        logger.info("testing");
      }
    } catch(Exception e){
      logger.error("Exception : ", e);
    }
  }

  @OnWebSocketMessage
  public void onMessage(String call) {
    logger.info("Received a new call! :: {}", call);
  }
}

class ReceiverThread extends Thread {
  private Session session;
  private static final Logger logger = LoggerFactory.getLogger(ReceiverThread.class);
  @Override
  public void run() {
    try {
      while(session.isOpen()) {
        session.getRemote().sendString("Pong");
        Thread.sleep(5000);
      }
    } catch (Exception e) {
        logger.error("Exception : ", e);
    }
  }
  public void setSession(Session session) {
    this.session = session;
  }
}